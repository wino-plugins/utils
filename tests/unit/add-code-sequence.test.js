const {expect} = require('expect')
const { addCodeSequentially } = require("../../src/math.utils");

async function test(){
   try {
      describe('Add code sequentially', function() {
         it('should accept lower case letters and return an uppercase code', (done) => {
            let code = addCodeSequentially('ab189a');
            expect(code).toEqual('AB189B')
            done()
         })

         it('should add one to zeros', (done) => {
            let code = addCodeSequentially('00000');
            expect(code).toEqual('00001')
            done()
         })

         it('should add carry one correctly', (done) => {
            let code = addCodeSequentially('0000Z');
            expect(code).toEqual('00010')
            done()
         })

         it('should add carry one for multiple codes', (done) => {
            let code = addCodeSequentially('00ZZZ');
            expect(code).toEqual('01000')

            code = addCodeSequentially('0ZZZZ');
            expect(code).toEqual('10000')

            code = addCodeSequentially('AB12Z9');
            expect(code).toEqual('AB12ZA')
            done()
         })

         it('should work for different code lengths', (done) => {
            let code = addCodeSequentially('ACFERT7D');
            expect(code).toEqual('ACFERT7E')

            code = addCodeSequentially('QW245648ACFERT7H');
            expect(code).toEqual('QW245648ACFERT7I')
            done()
         })

         it('should reset to zero if limit is exceeded', (done) => {
            let code = addCodeSequentially('ZZZZZ');
            expect(code).toEqual('00000')
            done()
         })

         it('should work with codes that have unknown characters', (done) => {
            let code = addCodeSequentially('ZZ_ZZA');
            expect(code).toEqual('ZZ_ZZB')
            
            code = addCodeSequentially('ZA_ZZZ');
            expect(code).toEqual('ZB_000')

            code = addCodeSequentially('AC_ZZZ');
            expect(code).toEqual('AD_000')

            code = addCodeSequentially('AC/ZZ0/ZZZ');
            expect(code).toEqual('AC/ZZ1/000')

            code = addCodeSequentially('DE AS3 54E');
            expect(code).toEqual('DE AS3 54F')

            code = addCodeSequentially('DE AS3 ZZZ');
            expect(code).toEqual('DE AS4 000')
            done()
         })

         // it('should set the environment to test', (done) => {
         //    expect(configs.env).toEqual('test')
         //    done()
         // })
      });
   } catch (err) {
      throw err
   }
}


module.exports = {
   test,
}