function generateRandomString(length = 7, chars){
   let str = ''
   if(!chars){
      chars = '1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz'
   }
   chars = chars.split('')

   for (let i = 0; i < length; i++) {
      str += chars[Math.floor(Math.random() * chars.length)]
   }

   return str;
}

function template(msg, data) {
   return msg.replace(
      /{(\w*)}/g, // or /{(\w*)}/g for "{this} instead of %this%"
      function (m, key) {
         if (data) {
            return data.hasOwnProperty(key) ? data[key] : "";
         }

         return data
      }
   );
}

function parsePhoneNumber(phoneNumber) {
   try {
      // Validate phone
      if(!phoneNumber?.mobile || !phoneNumber?.code?.calling_code || !phoneNumber?.code?.country_code){ if(ErrorHandler) { ErrorHandler.error('001-14', { phone_number: phoneNumber}) } else { throw 'Invalid phone number' } }
      // Check phone number
      if(phoneNumber?.code?.calling_code.includes('+')) { phoneNumber.code.calling_code = phoneNumber.code.calling_code.split('+').join('') }

      // Remove initial zero
      if(phoneNumber.mobile.startsWith('0') && phoneNumber.mobile.length > 9){
         phoneNumber.mobile =  phoneNumber.mobile.substring(1)
      }
      
      // Add a phone field
      phoneNumber.phone = `${phoneNumber.code.calling_code}${phoneNumber.mobile}`
      return phoneNumber
   } catch (err) {
      throw err
   }
}

module.exports = {
   generateRandomString,
   template,
   parsePhoneNumber
}