const nodeFetch = require('node-fetch');
let options = {
   mode: 'cors',
   headers: {}
}

function setOptions(data, opt) {
   Object.keys(opt).forEach(function (key) {
      data[key] = opt[key]
   })

   return data
}

function isJSON(data) {
   if (data !== null && data !== undefined && /^[\],:{}\s]*$/.test(data.replace(/\\["\\\/bfnrtu]/g, '@').
      replace(/"[^"\\\n\r]*"|true|false|null|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?/g, ']').
      replace(/(?:^|:|,)(?:\s*\[)+/g, ''))) {
      return true;
   }

   return false
}

function fetch(route, data, opt) {
   return new Promise(async (resolve, reject) => {
      try {
         if (Object.keys(data).length > 0) {
            data = JSON.stringify(data)
         } else {
            data = undefined
         }

         let req = setOptions({ body: data }, options)
         req = setOptions(req, opt)

         if (req.method === 'POST') {
            req.headers['Content-Type'] = 'application/json'
         }
         if (opt) {
            req = setOptions(req, opt)
         }

         let res = await nodeFetch(route, req)
         let body = await res.text()

         if (body === '') { body = undefined }
         if (isJSON(body)) {
            body = JSON.parse(body)
         }

         if (!res.ok) {
            throw body
         }

         resolve(body)
      } catch (err) {
         reject(err)
      }
   })
}

module.exports = {
   fetch,
   isJSON
}