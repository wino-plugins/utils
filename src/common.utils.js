function sleep(seconds = 1){
   return new Promise((resolve,reject) => {
      try {
         setTimeout(() => {
            resolve(true)
         }, seconds * 1000);

      } catch (err) {
         reject(err)
      }
   })
}

function convertArrayElementToObjectID(ids, key){
   try {
      if(key){
         return ids.map((el) => { 
            el[key] = Mongoose.Types.ObjectId(el[key])
            return el
         })
      }else{
         return ids.map((el) => { return Mongoose.Types.ObjectId(el)})
      }
   } catch (err) {
      throw err
   }
}


function safeObjectCopy(copyObject, newObject = {}) {
   // Don't do anything if add isn't an object
   if (!copyObject || typeof copyObject !== 'object') return newObject;
   var keys = Object.keys(copyObject);
   var i = keys.length;

   while (i--) {
      newObject[keys[i]] = copyObject[keys[i]];
   }
   return newObject;
}

async function validateExcelFields(data, fields){
   try {
      for (const field of fields) {
         if(!data[field]){
            if(ErrorHandler) { ErrorHandler.error('001-14', {fields, first_record: data}) } else { throw 'Invalid excel file data' }
         }
      }
   } catch (err) {
      throw err
   }
}

module.exports = {
   sleep,
   convertArrayElementToObjectID,
   safeObjectCopy,
   validateExcelFields,
}