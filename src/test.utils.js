'use strict';

const { v4 } = require('uuid');

async function getGeneratorObject(req){
   try {
      let ctx = { req_id: v4(), req, res: null};

      async function* genfunk() {
         yield;
      }

      let generator = await genfunk.call( ctx );

      // Now, let's inject a convenience method into the Generator Object that will allows us to change the prototype chain at execution time.
      generator.bindTo = function( new_ctx ) {
         ctx = new_ctx
      };

      return ctx;
   } catch (err) {
      throw err
   }
}

async function findServiceRequest(func){
   try {
      let results =  [];

      for (const service_func of OutgoingRPCs) {
         if(func === service_func?.func){ results.push(service_func); }
      }

      return results;
   } catch (err) {
      throw err
   }
}

module.exports = {
   getGeneratorObject,
   findServiceRequest
}