const path = require('path')
const fs = require('fs');

function getRootDirectory() {
   return path.join(__dirname, '../')
}

async function createDirectory(dir){
   try {
      await fs.promises.mkdir(dir, { recursive: true })
      return true
   } catch (err) {
      throw err
   }
}

function readFile(path,encoding = 'utf-8'){
   return new Promise((resolve,reject) => {
      try {
         fs.readFile(path,{encoding}, function(err,data) {
            if(err){
               reject(err)
            }
            
            resolve(data.toString())
         })

      } catch (err) {
         reject(err)
      }
   })
}

function writeFile(path,data,encoding='utf-8'){
   return new Promise((resolve,reject) => {
      try {
         fs.writeFile(path,data,encoding,(err) => {
            if(err){
               reject(err)
            }

            resolve(true)
         })
      } catch (err) {
         reject(err)
      }
   })
}

function readFileJSON(path,encoding = 'utf-8'){
   return new Promise((resolve,reject) => {
      try {
         fs.readFile(path,{encoding}, function(err,data) {
            if(err){
               reject(err)
            }
            
            resolve(JSON.parse(data.toString()))
         })

      } catch (err) {
         reject(err)
      }
   })
}

function writeFileJSON(path,data,encoding = 'utf-8',spacing = 3){
   return new Promise((resolve,reject) => {
      try {
         fs.writeFile(path,JSON.stringify(data,null,spacing),encoding,(err,data) => {
            resolve(data)
         })
      } catch (err) {
         reject(err)
      }
   })
}

function getFileStats(path){
   return new Promise((resolve,reject) => {
      try {
         fs.stat(path, function(err, stats) {
            if(err){
               resolve(null)
            }

            resolve(stats)
         });
      } catch (err) {
         // reject(err)
         resolve(null)
      }
   })
}

module.exports = {
   getRootDirectory,
   createDirectory,
   readFile,
   writeFile,
   readFileJSON,
   writeFileJSON,
   getFileStats
}