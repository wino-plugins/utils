function generateRandomNumber(length) {
   let num = ''

   for (i = 0; i < length; i++) {
      num += Math.round(Math.random() * 9)
   }

   return num
}

function addCodeSequentially(initial_sequence, alphabets = `0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ`){
   try {
      if(typeof initial_sequence !== 'string') { throw 'Invalid sequence ID' }
      initial_sequence = initial_sequence.toUpperCase()
      alphabets = alphabets.split('')
      initial_sequence = initial_sequence.split('')
      let added_sequence = '', carry_one = false

      for (let i = (initial_sequence?.length - 1); i >= 0; i--) {
         let index = findLetterIndex(alphabets, initial_sequence[i])
         let letter = initial_sequence[i]

         if(index === undefined){
            added_sequence = letter + added_sequence
         }else{
            if(carry_one){ 
               index++
               letter = alphabets[index]; 
               carry_one = false 
            }
            if(i === (initial_sequence?.length - 1)){
               index++
               
               if(index <= (alphabets.length - 1)){
                  letter = alphabets[index]
               }
            }
   
            if(index > (alphabets.length - 1)){ 
               carry_one = true; 
               letter = alphabets[index - alphabets.length]
            }
            added_sequence = letter + added_sequence
         }
         
      }
      
      return added_sequence
   } catch (err) {
      throw err
   }
}

// TODO: Add unit tests here
async function generateDBSequence(db_collection,alphabets){
   try {
      const { CodeSequence } = require(`${rootDir}/components/database/MongoDB/models`)

      if(!CodeSequence) { throw 'Operation not supported' }
      let sequence = await CodeSequence.findOne({db_collection})
      if(!sequence) { throw 'Initial sequence not defined' }

      let previous_sequence = sequence.last_sequence
      if(!sequence.last_sequence){ previous_sequence = sequence.initial_sequence }
      sequence.last_sequence = addCodeSequentially(previous_sequence,alphabets)

      // Reset and add code length if initial is found again
      if(sequence.last_sequence === sequence.initial_sequence){
         sequence.initial_sequence = addCodeSequentially(`${sequence.last_sequence}0`,alphabets)
         sequence.last_sequence = sequence.initial_sequence
      }

      sequence.last_update = new Date()
      await sequence.save()

      return sequence.last_sequence
   } catch (err) {
      throw err
   }
}

function findLetterIndex(alphabets, letter){
   try {
      for (let i = 0; i < alphabets.length; i++) {
         if(alphabets[i] === letter) { return i }
      }
   } catch (err) {
      throw err
   }
}

module.exports = {
   generateRandomNumber,
   addCodeSequentially,
   generateDBSequence
}